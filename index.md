---
layout: default
---
<h1 style="display: inline">Welcome</h1> to Monty's Practical D&D Guide! This is a D&D site for both beginners and already experienced players / DMs: It's a resource for those who want to go above and beyond. Here you'll find resources, papers, statsheets, and personal notes containing fun, dramatic, and helpful tidbits to enhance whatever fantastical situation you find yourself in!

There are lots of guides and utilities on this site, just click on a category in the sidebar to the left to see them. You can also see a list of useful sub-sites below.

***


<h2 style="display: inline">Worlds</h2><h5 style="display: inline"> - <a href="http://worlds.dnd.guide">worlds.dnd.guide</a></h5>
Worlds is a small universe - several, in fact - all contained within one little website.
Here you can find tons of information on worldbuilding and pre-made worlds, both for use in RPGs and just for the fun of it!
