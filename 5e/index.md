---
layout: default
---

# D&D 5e Resources and Tools.
In the "5e" dropdown to the left you can find any relevant utilities and pages.

***

### Credits
The following people deserve a mention for their help in contributing to this section:

 -  Monty : Programming
